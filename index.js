
const {SlashCommandBuilder} = require('@discordjs/builders');
const {REST} = require('@discordjs/rest');
const {Routes} = require('discord-api-types/v9');
const {Client, Intents, Collection} = require('discord.js');
const {guildID,clientID} = require('./config.json');
const {token} = require('./token.json')
const fs = require('node:fs');
//console.log(token)
const client = new Client({ intents: [Intents.FLAGS.GUILDS,Intents.FLAGS.GUILD_MEMBERS,Intents.FLAGS.GUILD_MESSAGES]});

client.commands = new Collection();
const commandFiles= fs.readdirSync('./commands').filter(file => file.endsWith('.js'));
for(const f of commandFiles){
    const command=require(`./commands/${f}`)
    client.commands.set(command.data.name,command);
}

const eventFiles=fs.readdirSync('./events').filter(file=>file.endsWith('.js'));
for(const f of eventFiles){
    const event=require(`./events/${f}`);
    if(event.once){
        client.once(event.name,(...args)=>event.execute(args));
    }
    else{
        client.on(event.name,async (...args)=>event.execute(...args));
    }
}
/*
client.once('ready',()=>{
    console.log('Client Loaded');
});

client.on('interactionCreate', async interaction =>{
    if(!interaction.isCommand()) return;
    const command = client.commands.get(interaction.commandName);
    if(!command) return;
    try{await command.execute(interaction)}
    catch(error){console.log(error);await interaction.reply({content:'Excecution Error',ephemeral:true})}


});
*/

client.login(token);