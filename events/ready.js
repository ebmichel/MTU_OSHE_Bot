const fs = require('node:fs');
module.exports={
    name:'ready',
    once:true,
    execute(client){
        delete require.cache[require.resolve('../config.json')]
        const c = require('../config.json');
        c["clientID"]=client[0].user.id
        fs.writeFile('config.json',JSON.stringify(c),function(err){if(err) throw err;console.log(`Client ID:${c["clientID"]}`)})
        console.log('Client Loaded')
    },

};