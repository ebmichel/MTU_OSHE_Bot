const { MessageActionRow, MessageSelectMenu } = require('discord.js');
const fs=require('node:fs')
const wait = require('node:timers/promises').setTimeout;
module.exports={
    name:'interactionCreate',
    async execute(interaction){
        delete require.cache[require.resolve('../roles.json')]
        const r = require('../roles.json');
        if(!interaction.isSelectMenu()) return;
        let selected=''
        if(interaction.customId==='select'){
            selected=parseInt(interaction.values[0])
            const opt=[]
                    for(i in r){
                        opt.push({label:`${r[i]['role']}`,value:i})
                    }
            opt[selected]['default']=true
            const permOptions=['User Selectable','Requires Approval']
            p=[]
            for(i in permOptions){
                p.push({label:`${permOptions[i]}`,value:i})
            }
            const perm = permOptions[parseInt(r[selected]['perm'])]
            const row=new MessageActionRow().addComponents(new MessageSelectMenu().setCustomId('select').setPlaceholder(r[selected]['role']).addOptions(opt))
            const permRow=new MessageActionRow().addComponents(new MessageSelectMenu().setCustomId('permRow').setPlaceholder(`${perm}`).addOptions(p))
            try{
                await interaction.deferUpdate();
                await wait(250);
                await interaction.editReply({content:`Role:${r[selected]['role']}\nPermission:${perm}`,components:[row,permRow],ephemeral:true})}
            catch(error){console.log(error);await interaction.update({content:'Excecution Error',ephemeral:true})}
            }
        if(interaction.customId==='permRow'){
            selected=parseInt(interaction.values[0])
            //console.log(interaction.message.components[0])
            sel=interaction.message.components[0].components[0].placeholder
            console.log(sel)
            const permOptions=['User Selectable','Requires Approval']
            p=[]
            for(i in permOptions){
                p.push({label:`${permOptions[i]}`,value:i})
            }
            p[selected]['default']=true
            const permRow=new MessageActionRow().addComponents(new MessageSelectMenu().setCustomId('permRow').addOptions(p))

            for(i in r){
                if(r[i]['role']===sel){
                    r[i]['perm']=selected
                    toWrite=JSON.stringify(r)
                    fs.writeFile('roles.json',toWrite,err=>{
                        if(err){console.log("error")}
                        //console.log(toWrite)
                        console.log("Writing to File")
                        })
                        try{
                            await interaction.deferUpdate();
                            await wait(250);
                            await interaction.editReply({content:`Role:${sel}\nPermission:${permOptions[selected]}`,components:[interaction.message.components[0],permRow]})
                        }
                        catch(error){console.log(error);await interaction.update({content:'Excecution Error',ephemeral:true})}

                }
            }
            
        }
    }
}