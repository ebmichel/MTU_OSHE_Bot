const { MessageActionRow, MessageSelectMenu } = require('discord.js');
const {guildID,clientID} = require('../config.json');

module.exports={
    name:'guildMemberAdd',
    async execute(member){
        delete require.cache[require.resolve('../config.json')]
        const c = require('../config.json')
        channelId=c['channelId']
        console.log("member joined")
        memberId=member.id;
        delete require.cache[require.resolve('../roles.json')]
        const r = require('../roles.json');
        const opt=[]
        const optAsk=[]
        for(i in r){
            if(r[i]['perm']===0){
                opt.push({label:`${r[i]['role']}`,value:i})
            }
            if(r[i]['perm']===1){
                optAsk.push({label:`${r[i]['role']}`,value:i})
            }
        }
        //console.log(opt)
        const g=await member.client.guilds.fetch(guildID);
        const guildName=g.name;
        const channel=await g.client.channels.cache.get(channelId)
        const rowRole=new MessageActionRow().addComponents(new MessageSelectMenu().setCustomId('selectdm').setPlaceholder('Roles').addOptions(opt).setMinValues(1))
        const rowAsk=new MessageActionRow().addComponents(new MessageSelectMenu().setCustomId('selectask').setPlaceholder('Roles').addOptions(optAsk).setMinValues(1))
        
        try{
            await console.log(member)
            await member.client.users.fetch(memberId,false).then((user)=>{user.send({content:`\nWelcome to ${guildName}, select a role in the first dropdown or request to be added to a role with the second dropdown (you can select multiple).`,components:[],ephemeral:true})})    
            await member.client.users.fetch(memberId,false).then((user)=>{user.send({content:"Select Roles to be assigned",components:[rowRole],ephemeral:true})})    
            await member.client.users.fetch(memberId,false).then((user)=>{user.send({content:"\nRequest Roles to be assigned",components:[rowAsk],ephemeral:true})}) 
        }
        catch(error){console.log(error);await channel.send({content:`Issue sending welcome message to ${member.id}`})}
    }
}