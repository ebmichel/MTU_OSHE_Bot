
const {SlashCommandBuilder} = require('@discordjs/builders');
const {REST} = require('@discordjs/rest');
const {Routes} = require('discord-api-types/v9');
const {Client, Intents} = require('discord.js');
const {token, guildID,clientID} = require('./config.json');
const fs = require('node:fs');

const commands=[]

const commandFiles= fs.readdirSync('./commands').filter(file => file.endsWith('.js'));
for(const f of commandFiles){
    const command=require(`./commands/${f}`)
    commands.push(command.data.toJSON());
}

const rest = new REST({ version: '9'}).setToken(token)
rest.put(Routes.applicationCommands(clientID),{body:[]})
rest.put(Routes.applicationGuildCommands(clientID,guildID),{body: commands})
    .then(()=>console.log("Commands Registered"))
    .catch(console.error);