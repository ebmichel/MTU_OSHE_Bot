const { MessageActionRow, MessageSelectMenu, MessageButton, MessagePayload } = require('discord.js');
const fs=require('node:fs')
const {Permissions} = require('discord.js')
const wait = require('node:timers/promises').setTimeout;
const {guildID,clientID} = require('../config.json');
module.exports={
    name:'interactionCreate',
    async execute(interaction){
        delete require.cache[require.resolve('../roles.json')]
        const r = require('../roles.json');
        //console.log(!interaction.isSelectMenu()&&!interaction.isButton())
        if(!interaction.isSelectMenu()&&!interaction.isButton()) return;
        
        if(interaction.customId==='selectdm'){
            console.log(interaction.values);
            let res=interaction.values;
            res=res.map(x=>parseInt(x));
            memberId=interaction.user.id;
            const g=await interaction.client.guilds.fetch(guildID);
            const gm=await g.members.fetch(memberId);
            console.log(gm);
            let added='Roles Added:';
            await interaction.deferUpdate();
            for(const i of res){
            try{
                await gm.roles.add(r[i]['id']);
                await wait(250);
                added+=`\n${r[i]['role']}`
            }
            catch(err){console.log(err);await interaction.editReply({content:`Error adding role ${r[i]['role']}, please contact admin`,components:[]})};
            }
            try{
                interaction.followUp({content:`${added}`,components:[]});
            }
            catch(err){console.log(err);await interaction.editReply({content:'Error, please contact admin',components:[]})};
        }
        if(interaction.customId==='selectask'){
            delete require.cache[require.resolve('../config.json')]
            const c = require('../config.json');
            channelId=c['channelId']
            let res=interaction.values;
            res=res.map(x=>parseInt(x));
            let memberId=interaction.user.id;
            const g=await interaction.client.guilds.fetch(guildID);
            const channel=await g.client.channels.cache.get(channelId)
            let foreMessage=`User ${interaction.user} is requesting to be given the following role/s:`
            let message =``
            for(const i of res){
                message+=`\n${r[i]['role']}`
            }
            const row = new MessageActionRow().addComponents(new MessageButton().setCustomId('buttongive').setLabel(`Give Roles to ${memberId}`).setStyle(`SUCCESS`))
            await channel.send({content:foreMessage});
            await channel.send({content:message,components:[row]});
            await interaction.reply(`Request Sent`)
        }
        if(interaction.customId==='buttongive'){
            let requester=await interaction.member.permissions.has(Permissions.FLAGS.MANAGE_ROLES)
            console.log(requester)
            if(requester){
                delete require.cache[require.resolve('../roles.json')]
                const r = require('../roles.json');
                let form = {}
                for (let i in r){
                    form[r[i]['role']]=r[i]['id']
                }
                const userID = interaction.component.label.split(' ').pop();
                rawContent=interaction.message.content
                let content = rawContent.split('\n')
                const gm = await interaction.guild.members.fetch(userID);
                console.log(gm)
                await interaction.deferUpdate();
                let added='Roles Added:'
                for (let role of content){
                    try{
                        console.log(form[role])
                        await gm.roles.add(form[role])
                        added+=`\n ${role}`
                        await(250);
                    }
                    catch(err){console.log(err);await interaction.followUp({content:`An Error occured, please manually add the roles`})}
                }
                interaction.editReply({content:added,components:[]})
                console.log(content)
            }
            else{interaction.reply({content:`You do not have permission to perform this command.\n If you think this is a mistake please reach out to the admin.`,ephemeral:true})}
        }
    }
}