# MTU OSHE Bot


## Description
This bot, written in JS using [discord.js](https://github.com/discordjs), is designed to allow for automation of role assignment for new and existing discord guild members. 

Users receive recieve a DM upon joining the guild in which they can select from a list of roles to be assigned, or request to have restricted roles assigned. 


## Installation
MTU OSHE Bot requires [nodejs](https://nodejs.org/en/)
Follow the instructions from discordjs to [create an application](https://discordjs.guide/preparations/setting-up-a-bot-application.html#creating-your-bot), and to [add the bot](https://discordjs.guide/preparations/adding-your-bot-to-servers.html#bot-invite-links) to your discord server. 
Suggested Permission Scope 

- Manage Server
- Manage Roles
- Read Messages/View Channels
- Manage Events
- Moderate Members
- Send Messages
- Manage Messages
- Read Message History
- Mention Everyone
- Use Slash Commands

URL should contain a permissions integer of `1213596904480`
Ensure that  all privileged gateway intents for your bot are enabled.
![Intents](assets/intents.png)

Download the repo
```
git clone https://gitlab.com/ebmichel/MTU_OSHE_Bot
cd MTU_MOST_Bot
```
Generate a token under the Bot tab in the Discord Developer portal. This token is used to authenticate your bot and should not be shared. 

Create a json in the main directory called `token.json`
In `token.json` write
```
{"token":"token string"}
```
where token string is the token generated in the portal.

Install NPM packages
```
npm install 
```

## Usage
To launch the bot
```
node index.js
```
Add the bot to the server using the URL generated during installation
Once the bot is added to your server, ensure that its role is placed above any role that it will need to be able to assign.

![role order](assets/role.png)

Note that the bot will still index all roles as if they can be assigned regardless of their actual positions. 

# <b>All commands except for `/roles getrole` are restricted to users with permission to manage roles, ensure that only those you wish to be able to interact with the bot have this permission.</b>

To initialise the bot type 
```
/init
```
in the discord server, the bot should respond. 

![init](assets/init.png)

Assign a channel for the bot to communicate with administrators in. While this can be a public channel it's not reccomended. 
```
/roles setbotchannel 
```
![set bot channel](assets/channel.png)

Retrieve the existing roles on the server 
```
/roles retrieve
```

![retrieve](assets/retrieve.png)

Note that this command will need to be used everytime a role is created or destroyed, and will resest the selectability permission for the bot. 

Assign selectability permissions for the bot.
```
/roles fetch
```
Select a role from the dropdown, and then select either `Requires Approval` or `User Selectable`

![perms](assets/fetch.png)

`Requires Approval` is the default, and means the the admin will be asked before the role is given to the user.

`User Selectable` means that the user can be assigned this role without explicit approval. 

A message is sent to users when they first join the server or by running the command 
```
/roles getroles
```
In the user's DMs they will receive the following

![dm message](assets/roles_select_message.png)

In the first dropdown the user can select roles to be assigned. Multiple roles can be selected at once in the dropdown. When the dropdown is closed the roles will be assigned.

![dm first](assets/user_selected_roles.png)

In the second dropdown the user can request roles to be assigned. Roles selected in this list then get sent to the previously declared bot channel. 

![dm request](assets/request_dm.png)

In the bot channel a message listing the requested roles is sent.

![dm bot channel](assets/request_bot_channel.png)

By clicking the button the listed roles will be assigned.

![dm bot channel granted](assets/request_bot_channel_granted.png)

## Recommended Use Case

After adding and setting up the bot create a welcome channel with information on how to request a role, informing them that they should have received a DM, and if they didn't to use the slash command `/roles getrole`. You can then restrict the access of the @everyone role to only be able to access that channel. Then ensure that all other roles have access to the channels they need access to. Make sure to assign the selectability for each role in the bot. By default all roles will require permission to be assigned. 

<b>Note: The bot has to be running whenever a command is used, and when a user joins for the message to be sent.</b>

