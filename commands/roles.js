const {SlashCommandBuilder} = require('@discordjs/builders');
const { GuildExplicitContentFilter } = require('discord-api-types/v10');
const {Permissions} = require('discord.js')
const { MessageActionRow, MessageSelectMenu } = require('discord.js');
const fs = require('node:fs');

module.exports={
    data :
        new SlashCommandBuilder()
        .setName('roles')
        .setDescription("Retrieve, Fetch or Set Guild Roles")
        .addSubcommand(subcommand=>
            subcommand
                    .setName('getrole')
                    .setDescription('Used to obtain roles'))
        .addSubcommand(subcommand=>
            subcommand
                .setName('retrieve')
                .setDescription('Fetches all roles and sets access permissions to admin only'))
        .addSubcommand(subcommand=>
            subcommand
                .setName('fetch')
                .setDescription('Prints currently registered roles and their permissions'))
        .addSubcommand(subcommand=>
            subcommand
                    .setName('setbotchannel')
                    .setDescription('Sets the channel for interactions with the bot')),
    async execute(interaction){
        let requester=await interaction.member.permissions.has(Permissions.FLAGS.MANAGE_ROLES)
        console.log(requester)
        if(interaction.options.getSubcommand()==='retrieve'){
            if(requester){
                interaction.guild.roles.fetch()
                    .then(roles=>{
                        let r=``;
                        let rolesArray=[];
                        roles.forEach(role=>{
                            //console.log(role.name);
                            if(role.name!=interaction.client.user.username&&role.name!='@everyone'){
                                r = r.concat(`\t`+String(role.name)+`\n`);
                                rolesArray.push({role:role.name,perm:1,id:role.id})
                            }
                            //console.log(r)
                        })
                        roleJSON=JSON.stringify(rolesArray)
                        fs.writeFile('roles.json',roleJSON,function (err){if (err) throw err;console.log("Roles Saved")});
                        delete require.cache[require.resolve('../roles.json')]
        
        
                        interaction.reply({content:`Created Roles:\n${r}`, ephemeral:true});
                    })
                    .catch(console.error);
            }
            else{interaction.reply({content:`You do not have permission to perform this command.\n If you think this is a mistake please reach out to the admin.`,ephemeral:true})}
        }
        if (interaction.options.getSubcommand()==='fetch'){
            if(requester){
                delete require.cache[require.resolve('../roles.json')]
                const r = require('../roles.json');
                console.log(r[0]['role'])
                const opt=[]
                for(i in r){
                    opt.push({label:`${r[i]['role']}`,value:i})
                }
                //console.log(opt)
                const row=new MessageActionRow().addComponents(new MessageSelectMenu().setCustomId('select').setPlaceholder('Roles').addOptions(opt))
                await interaction.reply({content:"Select a Role",components:[row],ephemeral:true})
            }
            else{interaction.reply({content:`You do not have permission to perform this command.\n If you think this is a mistake please reach out to the admin.`,ephemeral:true})}

        }
        if (interaction.options.getSubcommand()==='getrole'){
            delete require.cache[require.resolve('../roles.json')]
            const r = require('../roles.json');
            const opt=[]
            const optAsk=[]
            for(i in r){
                if(r[i]['perm']===0){
                    opt.push({label:`${r[i]['role']}`,value:i})
                }
                if(r[i]['perm']===1){
                    optAsk.push({label:`${r[i]['role']}`,value:i})
                }
            }
            //console.log(opt)
            guildName=interaction.guild.name;
            memberId=interaction.member.id;
            const rowRole=new MessageActionRow().addComponents(new MessageSelectMenu().setCustomId('selectdm').setPlaceholder('Roles').addOptions(opt).setMinValues(1))
            const rowAsk=new MessageActionRow().addComponents(new MessageSelectMenu().setCustomId('selectask').setPlaceholder('Roles').addOptions(optAsk).setMinValues(1))
            await interaction.client.users.fetch(memberId,false).then((user)=>{user.send({content:`\nWelcome to ${guildName}, select a role in the first dropdown or request to be added to a role with the second dropdown (you can select multiple).`,components:[],ephemeral:true})})
            await interaction.client.users.fetch(memberId,false).then((user)=>{user.send({content:"Select Roles to be assigned",components:[rowRole],ephemeral:true})})
            await interaction.client.users.fetch(memberId,false).then((user)=>{user.send({content:"\nRequest Roles to be assigned",components:[rowAsk],ephemeral:true})})
            await interaction.reply({content:`Message Sent`,ephemeral:true})
        }
        if (interaction.options.getSubcommand()==='setbotchannel'){
            if(requester){
                delete require.cache[require.resolve('../config.json')]
                const c = require('../config.json');
                channelId=interaction.channelId;
                c['channelId']=channelId;
                fs.writeFile('config.json',JSON.stringify(c),function (err){if (err) throw err;console.log("Bot Channel Saved")});
                interaction.reply({content:`Bot Channel Defined as ${interaction.channel}\nID:${channelId}`,ephemeral:true});
            }
            else{interaction.reply({content:`You do not have permission to perform this command.\n If you think this is a mistake please reach out to the admin.`,ephemeral:true})}

        }
    }
        
}