const {Permissions} = require('discord.js')
const fs = require('node:fs');
const {REST} = require('@discordjs/rest');
const {Routes} = require('discord-api-types/v9');

module.exports={
    name:'messageCreate',
    async execute(message){
        //console.log(message)
        let requester=await message.member.permissions.has(Permissions.FLAGS.MANAGE_ROLES)
        if (requester){
            console.log(message.content)
            if(message.content==='/init'){
                console.log(message.guildId)
                delete require.cache[require.resolve('../config.json')]
                const {token} = require('../token.json');
                const {guildID,clientID} = require('../config.json');
                let c = require('../config.json');
                c['guildID']=message.guildId
                fs.writeFile('config.json',JSON.stringify(c),function (err){if(err) throw err;
                const commands=[]
                const commandFiles= fs.readdirSync('commands').filter(file => file.endsWith('.js'));
                for(const f of commandFiles){
                    const command=require(`../commands/${f}`)
                    commands.push(command.data.toJSON());
                }
                const rest = new REST({ version: '9'}).setToken(token)
                rest.put(Routes.applicationCommands(clientID),{body:[]})
                rest.put(Routes.applicationGuildCommands(clientID,guildID),{body: commands})
                    .then(()=>console.log("Commands Registered"))
                    .catch(console.error);
                message.reply({content:`Target Server set to ${message.guildId}`,ephemeral:true});

                    })
                }
        }
    },

};